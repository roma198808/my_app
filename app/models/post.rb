class Post < ActiveRecord::Base
	validates :title, :content, :presence => true
	validates :title, :content, :length => { :minimum => 5 }
	validates :title, :uniqueness => true
end
